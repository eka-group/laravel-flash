# A lightweight package to flash messages

[![Latest Version on Packagist](https://img.shields.io/packagist/v/spatie/laravel-flash.svg?style=flat-square)](https://packagist.org/packages/spatie/laravel-flash)
[![Build Status](https://img.shields.io/travis/spatie/laravel-flash/master.svg?style=flat-square)](https://travis-ci.org/spatie/laravel-flash)
[![StyleCI](https://github.styleci.io/repos/175572658/shield?branch=master)](https://github.styleci.io/repos/175572658)
[![Quality Score](https://img.shields.io/scrutinizer/g/spatie/laravel-flash.svg?style=flat-square)](https://scrutinizer-ci.com/g/spatie/laravel-flash)
[![Total Downloads](https://img.shields.io/packagist/dt/spatie/laravel-flash.svg?style=flat-square)](https://packagist.org/packages/spatie/laravel-flash)

This is a lightweight package to send flash messages in Laravel apps. A flash message is a message that is carried over to the next request by storing it in the session. This package only supports one single flash message at a time.

This is how it can be used:

```php
class MySpecialSnowflakeController
{
    public function store()
    {
        // …

        flash('My message', 'my-class');

        // or

        flash(['First message', 'Second message'], 'my-class');

        return back();
    }
}
```

In your view you can do this:

```blade
@if (flash()->message)
    <div class="{{ flash()->class }}">
        @if (!is_array(flash()->message))
            {!! flash()->message !!}
        @else
            <ul>
                @foreach (flash()->message as $msg)
                    <li>{!! $msg !!}</li>
                @endforeach
            </ul>
        @endif
    </div>
@endif
```

## Installation

You can install the package via composer:

```bash
composer require futuresoft/laravel-flash
```

## Usage

Here is an example on how to flash a message.

```php
class MyController
{
    public function store()
    {
        // …

        flash('My message');

        return back();
    }
}
```

In your view you can use it like this

```blade
@if(flash()->message)
    <div class="{{ flash()->class }}">
        @if (!is_array(flash()->message))
            {!! flash()->message !!}
        @else
            <ul>
                @foreach (flash()->message as $msg)
                    <li>{!! $msg !!}</li>
                @endforeach
            </ul>
        @endif
    </div>
@endif
```

You can also set an array of messages.

```php
flash(['First message', 'Second message', 'Third message']));
```

Here is the output:

```html
<ul>
    <li>First message</li>
    <li>Second message</li>
    <li>Third message</li>
</ul>
```

### Using a class name to style the displayed message

You can add a class as the second parameter. This is typically used to style the output in your HTML.

```php
class MyController
{
    public function store()
    {
        // …

        flash('My message', 'my-class');

        return back();
    }
}
```

In your view you can use the class like this:

```blade
@if (flash()->message)
    <div class="{{ flash()->class }}">
        @if (!is_array(flash()->message))
            {!! flash()->message !!}
        @else
            <ul>
                @foreach (flash()->message as $msg)
                    <li>{!! $msg !!}</li>
                @endforeach
            </ul>
        @endif
    </div>
@endif
```

You can also set an array of classes. These will be output by `flash()->class` by imploding the array with a space-delimiter.

```php
 flash('My message', ['my-class', 'another-class'])); // flash()->class output is: 'my-class another-class'
```


### Adding your own methods

If you don't want to specify a class each time you flash a message you can add a method name to `flash`.

The easiest way is by passing an array to the `levels` method. The key is the method name that should be added to `flash()`. The value is the class that will automatically be used when rendering the message.

```php
// this would probably go in a service provider

\FutureSoft\Flash\Flash::levels([
    'success' => 'alert-success',
    'warning' => 'alert-warning',
    'error' => 'alert-error',
]);
```

The above example will make these methods available on `flash`:

```php
flash()->success('Hurray');
flash()->warning('Mayybeee');
flash()->error('Oh Oh');
```

Additionally, when you've added your own method, you can also pass that method name as a second parameter to `flash` itself:

```php
flash('Hurray', 'success'); // `flash()->class` will output 'alert-success'
```

You can also add a method to `flash` by using `macro`.

Here's an example:

```php
// this would probably go in a service provider

use FutureSoft\Flash\Message;

\FutureSoft\Flash\Flash::macro('warning', function(string $message) {
    return $this->flashMessage(new Message($message, 'alert alert-warning'));
});
```

You can now use a `warning` method on `flash`:

```php
flash()->warning('Look above you!');
```

## Testing

``` bash
composer test
```

## Changelog

Please see [CHANGELOG](CHANGELOG.md) for more information on what has changed recently.

## Contributing

Please see [CONTRIBUTING](CONTRIBUTING.md) for details.

### Security

If you discover any security related issues, please email kodjosama@gmail.com instead of using the issue tracker.

## Credits

- [Kodjo Edem](https://github.com/kodjosama)
- [Freek Van der Herten](https://github.com/freekmurze)
- [All Contributors](../../contributors)

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.
