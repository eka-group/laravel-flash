<?php

use FutureSoft\Flash\Flash;
use FutureSoft\Flash\Message;

/**
 * @param array|string $text
 * @param array|string $class
 */
function flash($text = null, $class = null): Flash
{
    $flash = app(Flash::class);

    if (null === $text) {
        return $flash;
    }

    $message = new Message($text, $class);

    $flash->flash($message);

    return $flash;
}
